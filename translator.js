function calculate_result() {
    var base1 = parseInt(document.getElementById("from_input").value); 
    var base2 = parseInt(document.getElementById("to_input").value); 
    var s = document.getElementById("main_input").value;
    var arr = allowed_symbols.slice(base1, allowed_symbols.length);
    for (var i = 0; i < arr.length; i++)
        if (s.indexOf(arr[i]) != -1)
            return;
    var p = 0;
    if (s.indexOf('.') != -1) {
        p = s.length - s.indexOf(".") - 1;
    }
    s = s.replace(".", "");
    var num = parseInt(s, base1);
    num /= Math.pow(base1, p);
    var result = num.toString(base2);
    document.getElementById("result").getElementsByTagName("span")[0].innerText = result.toString().toUpperCase();
}


var allowed_symbols = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];
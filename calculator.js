function preprocess(el) {
    prev_cur_number = cur_number;
    var type = el.getAttribute("btn-type");
    console.log(type);
    var text = el.innerText;
    switch (type) {
        case "digit":
            if (typeof stack[stack.length - 1] == "number" && !cur_number) {
                add_mult();
            }
            var digit = parseInt(text);
            var sign = cur_number ? (cur_number / Math.abs(cur_number)) : 1;
            if (cur_number % 1 == 0)
                if (dot_tracker)
                    if (digit)
                        cur_number += (digit / Math.pow(10, dot_tracker)) * sign;
                    else
                        dot_tracker += 1;
                else
                    cur_number = cur_number * 10 + digit * sign;
            else
                if (dot_tracker){
                    var s = cur_number.toString();
                    var p = Math.pow(10, s.length - s.indexOf('.'));
                    cur_number *= p;
                    cur_number += sign * digit;
                    cur_number /= p;
                }
                else
                    cur_number = cur_number * 10 + digit * sign;
            calc_text.push(text);
            break;
        case "da_function":
            var op_name = el.getAttribute("op_name");
            if ((stack.length ? stack[stack.length - 1] : []).type == "da_function" && !cur_number) {
                calc_text = calc_text.slice(0, calc_text.length - 1);
                calc_text.push(op_name);
                stack = stack.slice(0, stack.length - 1);
                stack.push({"type": "da_function", "f_name": op_name});
                break;
            }
            if (cur_number) {
                stack.push(cur_number);
                dot_tracker = 0;
            }
            cur_number = 0;
            var to_push = {"type": "da_function", "f_name": op_name};
            stack.push(to_push);
            calc_text.push(op_name);
            break;
        case "function":
            console.log("function incoming");
            if (cur_number || stack[stack.length - 1].type == "subres") {
                if (cur_number)
                    stack.push(cur_number);
                dot_tracker = 0;
                cur_number = 0;
                add_mult();
            }
            f_name = el.getAttribute("f_name") || text;
            var to_push = {"type": "function",
                           "function": function_dict[f_name]}
            stack.push(to_push);
            if (f_name == "(")
                calc_text.push("(");
            else
                calc_text.push(f_name + "(");
            break;
        case "equal":
            cur_number = calculate_stack();
            if (cur_number % 1 != 0)
                dot_tracker = 1;
            var arr = cur_number.toString().split("");
            calc_text = [];
            for (var i = 0; i < arr.length; ++i)
                if (arr[i] == "." || arr[i] == "-")
                    calc_text.push(arr[i]);
                else
                    calc_text.push(parseInt(arr[i]));
            break;
        case "bracket":
            calculate_last_func();
            calc_text.push(")");
            cur_number = 0;
            break;
        case "clear":
            stack = [];
            cur_number = 0;
            calc_text = [];
            break;
        case "backspace":
            if (cur_number) {
                if (dot_tracker)
                    if (cur_number % 1 == 0)
                        dot_tracker = 0;
                    else {
                        var s = cur_number.toString();
                        cur_number = parseFloat(s.slice(0, s.length - 1));
                    }
                else
                    if (cur_number > 0)
                        cur_number = Math.floor(cur_number / 10);
                    else
                        cur_number = Math.ceil(cur_number / 10);
                calc_text.pop();
                break;
            }
            if (!stack.length)
                break;
            var last = stack.pop();
            console.log("last:");
            console.log(last);
            if (last.type == "subres")
                cur_number = last.last_num;
            calc_text.pop();
            if (typeof stack[stack.length - 1] == "number")
                cur_number = stack.pop();
            if (cur_number % 1 != 0)
                dot_tracker = 1;
            break;
        case "smile":
            console.clear();
            break;
        case "const":
            console.log("here");
            if (text == "e")
                var num = Math.E;
            else
                var num = Math.PI;
            if (cur_number) {
                stack.push(cur_number);
                dot_tracker = 0;
                cur_number = 0;
                add_mult();
            }
            calc_text.push(text);
            stack.push(num);
            break;
        case "2nd":
            var btns = document.getElementsByClassName("reversable");
            for (var i = 0; i < btns.length; ++i)
                if (btns[i].innerText[0] == "a")
                    btns[i].innerText = btns[i].innerText.slice(1);
                else
                    btns[i].innerText = "a" + btns[i].innerText;
            break;
        case "dot":
            calc_text.push(text);
            dot_tracker = 1;
            break;
        case "change_sign":
            s = cur_number.toString();
            if (cur_number > 0)
                calc_text = calc_text.slice(0, calc_text.length - s.length).concat(["-"], calc_text.slice(calc_text.length - s.length));
            else
                calc_text.splice(calc_text.length - s.length, 1);
            cur_number *= -1
            break;
        case "memory":
            switch (text) {
                case "MS":
                    memory = parseFloat(cur_number);
                    break;
                case "MC":
                    memory = null;
                    break;
                case "MR":
                    if (cur_number || dot_tracker)
                        throw "Something went wrong";
                    else {
                        cur_number = memory;
                        var s = memory.toString();
                        if (s.indexOf(".") != -1)
                            dot_tracker = 1;
                        calc_text = calc_text.concat(s.split(""));
                    }
                    memory = null;
                    break;
                case "M+":
                    if (memory == null)
                        break;
                    var cur_num_start = null;
                    if (comp_arrs(calc_text, cur_number.toString().split("")))
                        cur_num_start = -1;
                    else
                        for (var  i = calc_text.length - 1; i >= 0 && cur_num_start == null; --i)
                            if (!(/^\d+$/.test(calc_text[i]) || calc_text[i] == '.' || calc_text[i] == '-'))
                                cur_num_start = i;
                    console.log("cur_num_start: " + cur_num_start);
                    cur_number += parseFloat(memory);
                    calc_text = calc_text.slice(0, cur_num_start + 1).concat(cur_number.toString().split(""));
                    break;
                case "M-":
                    if (memory == null)
                        break;
                    var cur_num_start = null;
                    if (comp_arrs(calc_text, cur_number.toString().split("")))
                        cur_num_start = -1;
                    else
                        for (var  i = calc_text.length - 1; i >= 0 && cur_num_start == null; --i)
                            if (!(/^\d+$/.test(calc_text[i]) || calc_text[i] == '.' || calc_text[i] == '-'))
                                cur_num_start = i;
                    cur_number -= parseFloat(memory);
                    calc_text = calc_text.slice(0, cur_num_start + 1).concat(cur_number.toString().split(""));
                    break;

            }
        case "notation":
            if (!comp_arrs(calc_text, cur_number.toString().split(""))){
                throw "Something went wrong";
                break;
            }
            switch (text) {
                case "10->2":
                    var bin_s = cur_number.toString(2);
                    calc_text = bin_s.split("");
                    cur_number = parseFloat(bin_s);
                    break;
                case "2->10":
                    var p = 0;
                    if (dot_tracker) {
                        var s = cur_number.toString();
                        p = s.length - s.indexOf('.') - 1;
                        cur_number *= Math.pow(10, p);
                    }
                    var dec_n = parseInt(cur_number, 2) / Math.pow(2, p);
                    calc_text = dec_n.toString().split("");
                    cur_number = dec_n;
                    break;

            }
            break;

    }
    console.log("cur_number: " + cur_number);
    console.log(stack);
    if (isNaN(cur_number))
        cur_number = prev_cur_number;
    if (calc_text.length)
        document.getElementsByClassName("text-area")[0].innerText = calc_text.join("");
    else
        document.getElementsByClassName("text-area")[0].innerText = "\n";
}


function calculate_last_func() {
    var sum = cur_number;
    var found_func = false;
    var func_start = null; 
    stack.push(cur_number);
    for (var i = stack.length - 1; i >= 0 && !found_func; --i) {
        func = stack[i];
        switch (func.type) {
            case "subres":
                i = func.result;
                break;
            case "function":
                var sub_stack = stack.slice(i, stack.length);
                func_start = i;
                break;
        }
    }
    for (var  i = 0; i < sub_stack.length; i ++) {
        if (sub_stack[i].f_name == "^") {
            var res = Math.pow(sub_stack[i - 1], sub_stack[i + 1]);
            sub_stack = sub_stack.slice(0, i - 1).concat([res], sub_stack.slice(i + 2, sub_stack.length));
            --i;
        }
    }
    for (var  i = 0; i < sub_stack.length; i ++) {
        if (sub_stack[i].f_name == "*" || sub_stack[i].f_name == "/") {
            if (sub_stack[i].f_name == "*")
                var res = mult_two(sub_stack[i - 1], sub_stack[i + 1]);
            else
                var res = div_two(sub_stack[i - 1] / sub_stack[i + 1]);
            sub_stack = sub_stack.slice(0, i - 1).concat([res], sub_stack.slice(i + 2, sub_stack.length));
            --i;
        }
    }
    var sum = sub_stack.pop();
    for (var i = sub_stack.length - 1; i >= 0 && !found_func; --i) {
        func = sub_stack[i];
        switch (func.type) {
            case "da_function":
                --i;
                f_name = func.f_name;
                arg = sub_stack[i];
                if (arg.type == "subres") {
                    i = arg.start - 1;
                    arg = arg.result;
                    console.log(sub_stack);
                    console.log("arg from subres: " + arg);
                }
                if (typeof arg != "number" )
                    console.log("NaN before operator");
                switch (f_name) {
                    case "+":
                        sum = sum_two(sum, arg);
                        break;
                    case "-":
                        sum = sub_two(arg, sum);
                        break;
                    console.log("something went wrong")
                }
                break;
            case "function":
                f = func.function;
                to_push = {
                    "type": "subres",
                    "result": f(sum),
                    "start": func_start,
                    "last_num": cur_number,
                };
                stack.push(to_push);
                found_func = true;
                break;  
            case "subres":
                sum = func.result;
                i = func.start;
                break;            
        }
    }
}

function print_stack() {
    console.log("---------------------------------------------------------");
    for (i in stack){
        console.log(i + ": " + stack[i]);
        if (typeof stack[i] == "object") {
            for (j in stack[i])
                console.log("\t" + j + ": " + stack[i][j]);
        }
    }
    console.log("---------------------------------------------------------");
}


function calculate_stack() {
    var func, arg;
    for (var i = stack.length - 1; i >= 0; --i) {
        var elem = stack[i];
        if (elem.type == "subres") {
            stack = stack.slice(0, elem.start).concat([elem.result], stack.slice(i + 1, stack.length));
            i = elem.start;
        }
    }
    if (typeof stack[stack.length - 1] != "number") {
        stack.push(cur_number);
        dot_tracker = 0;
        cur_number = 0;
    }
    print_stack();
    for (var  i = 0; i < stack.length; i ++) {
        if (stack[i].f_name == "^") {
            var res = Math.pow(stack[i - 1], stack[i + 1]);
            stack = stack.slice(0, i - 1).concat([res], stack.slice(i + 2, stack.length));
            --i;
        }
    }
    for (var  i = 0; i < stack.length; i ++) {
        if (stack[i].f_name == "*" || stack[i].f_name == "/") {
            if (stack[i].f_name == "*")
                var res = mult_two(stack[i - 1], stack[i + 1]);
            else
                var res = div_two(stack[i - 1], stack[i + 1]);
            stack = stack.slice(0, i - 1).concat([res], stack.slice(i + 2, stack.length));
            --i;
        }
    }
    print_stack();
    if (typeof stack[stack.length - 1] == "number")
        cur_number = stack.pop()
    while (stack.length) {
        func = stack.pop();
        switch (func.type) {
            case "da_function":
                f_name = func.f_name;
                arg = stack.pop();
                if (arg.type == "subres") {
                    stack = stack.slice(0, arg.start);
                    arg = arg.result;
                }
                if (typeof arg != "number" )
                    console.log("NaN before operator");
                switch (f_name) {
                    case "+":
                        console.log("cur_num: " + cur_number + "\narg: " + arg + "\nstack: ");
                        cur_number = sum_two(cur_number, arg);
                        break;
                    case "-":
                        cur_number = syb_two(arg, cur_number);
                        break;
                    console.log("something went wrong");
                }
                break;
            case "function":
                f = func.function;
                cur_number = f(cur_number);
                break;

        }
    }
    return cur_number;
}

function sum_two(a, b) {
    console.log("in sum two");
    var s_a = a.toString();
    var s_b = b.toString();
    var p_a = Math.pow(10, s_a.length - s_a.indexOf('.') - 1);
    var p_b = Math.pow(10, s_b.length - s_b.indexOf('.') - 1);
    var p = Math.max(p_a, p_b);
    a *= p;
    b *= p;
    var res = a + b;
    if (p)
        res /= p;
    return res;
}


function sub_two(a, b) {
    var s_a = a.toString();
    var s_b = b.toString();
    var p_a = Math.pow(10, s_a.length - s_a.indexOf('.') - 1);
    var p_b = Math.pow(10, s_b.length - s_b.indexOf('.') - 1);
    var p = Math.max(p_a, p_b);
    a *= p;
    b *= p;
    var res = a - b;
    if (p)
        res /= p;
    return res;
}


function mult_two(a, b) {
    var s_a = a.toString();
    var s_b = b.toString();
    var p_a = Math.pow(10, s_a.length - s_a.indexOf('.') - 1);
    var p_b = Math.pow(10, s_b.length - s_b.indexOf('.') - 1);
    a *= p_a;
    b *= p_b;
    var res = a * b;
    res /= (p_a * p_b);
    return res;
}

function div_two(a, b) {
    var s_a = a.toString();
    var s_b = b.toString();
    var p_a = Math.pow(10, s_a.length - s_a.indexOf('.') - 1);
    var p_b = Math.pow(10, s_b.length - s_b.indexOf('.') - 1);
    a *= p_a;
    b *= p_b;
    var res = a / b;
    if (p_a && p_b)
        res /= (p_a / p_b);
    return res;
}

function process_keydown(e) {
    if (e.keyCode == 16) { 
        shift_pressed = true;
        return;
    }
    if (shift_pressed)
        var key_code = "Shift+" + e.keyCode;
    else
        var key_code = "" + e.keyCode;
    console.log("key code: " + key_code);
    var el = document.querySelector("[btn-code=\"" + key_code + "\"]");
    el.click();
}

function process_keyup(e) {
    if (e.keyCode == 16) 
        shift_pressed = false;
}

function add_mult(){
    calc_text.push("*");
    var to_push = {"type": "da_function", "f_name": "*"};
    stack.push(to_push);
}

function create_alert(type, text) {

}

function comp_arrs(array1, array2) {
    return array1.length === array2.length && array1.every(function(value, index) { return value === array2[index]});
}



var memory = null;
var dot_tracker = 0;
var shift_pressed = false;
var function_dict = {
    "sin": Math.sin,
    "asin": Math.asin,
    "cos": Math.cos,
    "acos": Math.acos,
    "tan": Math.tan,
    "atan": Math.atan,
    "sqrt": Math.sqrt,
    "1/": (x => 1 / x),
    "(": (x => x),
};
var preresult = 0;
var cur_number = 0;
var prev_cur_number = null;
var calc_text = [];
var stack = [];
